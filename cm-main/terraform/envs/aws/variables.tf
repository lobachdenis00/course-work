variable "ssh_key" {
  type    = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+7OsrpWo3V4khSGVwU7wyWq9m3qa2lpWnQVVmgKGwLAF8Z3cJGJ/vHTXsHrFZ2qZQZ2yB5qOwxCkjqqHQPtMcCEeLx7aEF/tVitPZEFGd5SPKq91r8b1AXTqVbxIx6bPgKWoWk/bjb5WiWmqd51faj/RJvY2wokAUDOYnkzDuhPptCuTuUUeef9wgjzh2+ctgEFl+9TttIf92CKtv9v2La/L6x00ID8tQLn8SkZwIGwOFrBZQckjOw3hqjOxJM558aKo686sKK5eWYNbfpAdm32ycWrVipf3GF4IQzz5L4Fr4yCoce/D04UXqJXN8zfFU9h9ErOvpKyq2XaDqnnJP root@devops-school"
}

variable "tags" {
  type = map(any)

  default = {
    Environment = "sandbox"
    ManagedBy   = "ansible"
    CreatedBy   = "Terraform"
  }
}

variable "az" {
  type    = string
  default = "eu-central-1a"
}
